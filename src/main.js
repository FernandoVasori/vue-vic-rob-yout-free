import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import LastArticles from './components/LastArticles';
import MiComponente from './components/MiComponente'
import HelloWorld from './components/HelloWorld'
import Blog from './components/Blog'
import Formulario from './components/Formulario'
import Pagina from './components/Pagina'
import ErrorComponent from './components/ErrorComponent'
import Peliculas from './components/Peliculas.vue'



// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
    // Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false
Vue.use(VueRouter);

const routes = [
    { path: '/home', component: LastArticles },
    { path: '/ultimos-articulos', component: LastArticles },
    { path: '/mi-componente', component: MiComponente },
    { path: '/hola-mundo', component: HelloWorld },
    { path: '/', component: LastArticles },
    { path: '/blog', component: Blog },
    { path: '/formulario', component: Formulario },
    { path: '/pagina/:id?', name: 'pagina', component: Pagina }, //signo de interrogacion hace quew el param sea opcional
    { path: '*', name: 'pagina', component: ErrorComponent }, // ruta que se carga cuando no existe , eso inidca el *
    { path: '/peliculas', name: 'peliculas', component: Peliculas }

];
const router = new VueRouter({
    routes,
    mode: 'history'

});

new Vue({
    router, // para que te  lo carge en la instancia de vue
    render: h => h(App),
}).$mount('#app');